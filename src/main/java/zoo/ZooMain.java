package zoo;

import java.util.Scanner;
import java.io.*;


public class ZooMain {

    public static void main(String[] args) {

        System.out.println("Hello to the ZOO");

        Scanner scanner = new Scanner(System.in);

        Animal[] animals = null;

        boolean run = true;

        System.out.println("Command:");
        System.out.println("add");
        System.out.println("print");
        System.out.println("print_all");
        System.out.println("save");
        System.out.println("read");
        System.out.println("quit");

        while (run) {

            String text = scanner.nextLine();

            switch (text) {

                case "add": {
                    System.out.println("podaj ilosc zwierzatek");
                    int numberAnimals = scanner.nextInt();
                    scanner.skip("\n");

                    animals = new Animal[numberAnimals];

                    for (int i = 0; i < animals.length; i++) {

                        System.out.println("Podaj rodzaj zwierzątka i naciśnij enter");
                        String type = scanner.nextLine();
                        System.out.println("Podaj imię zwierzątka i naciśnij enter");
                        String name = scanner.nextLine();
                        System.out.println("Podaj kolor zwierzątka i naciśnij eneter");
                        String color = scanner.nextLine();

                        if (type.equals("Wolf")) {
                            System.out.println("dodalem zwierze");
                            Wolf wolf = new Wolf();
                            wolf.setName(name);
                            wolf.setFurColor(color);
                            animals[i] = wolf;
                        }

                        if (type.equals("Parrot")) {
                            Parrot parrot = new Parrot();
                            parrot.setName(name);
                            parrot.setFeatherolor(color);
                            animals[i] = parrot;
                        }

                        if (type.equals("Iguana")) {
                            Iguana iguana = new Iguana();
                            iguana.setName(name);
                            iguana.setScaleColor(color);
                            animals[i] = iguana;
                        }
                    }
                }

                case "print": {
                    print(animals);
                }

                case "save": {
                    saveToFile(animals, "zoo");
                }

                case "read": {
                    animals = readFromFile("zoo");
                }

                case "quit": {
                    break;
                }
            }
        }
    }

    static void print(Animal[] animals) {

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Wolf) {
                System.out.println("Wolf:" + "\nNazwa zwierzaka: " + animals[i].getName() + "\nKolor= "
                        + ((Wolf) animals[i]).getFurColor());
            }

            if (animals[i] instanceof Parrot) {
                System.out.println("Parrot:" + "\nNazwa zwierzaka: " + animals[i].getName() + "\nKolor= "
                        + ((Parrot) animals[i]).getFeatherolor());
            }

            if (animals[i] instanceof Iguana) {
                System.out.println("Iguana:" + "\nNazwa zwierzaka: " + animals[i].getName() + "\nKolor= "
                        + ((Iguana) animals[i]).getScaleColor());
            }
        }
    }

    static void feed(Animal[] animals) {

        for (Animal e : animals) {
            e.eat();
        }
    }

    static void saveToFile(Animal[] animals, String file) {
        System.out.println("Zapisuje do pliku " + file);
        try (FileWriter fw = new FileWriter(file);
             BufferedWriter bw = new BufferedWriter(fw);) {
            int count = 0;
            for (Animal i : animals) {
                count++;
            }
            bw.write(count + "\n");
            for (Animal i : animals) {
                String nameClass = i.getClass().getSimpleName();
                bw.write(nameClass + " ");
                bw.write(i.getName() + " ");
                if (nameClass.equals("Wolf")) {
                    bw.write(((Wolf) i).getFurColor() + "\n");
                }
                if (nameClass.equals("Parrot")) {
                    bw.write(((Parrot) i).getFeatherolor() + "\n");
                }
                if (nameClass.equals("Iguana")) {
                    bw.write(((Iguana) i).getScaleColor() + "\n");
                }
            }
        } catch (IOException ex) {
            System.out.println("Nie zapisano - fatal crash error system dump i ch**");
        }
    }

    static void howl(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Wolf) {
                ((Wolf) i).howl();
            }
        }
    }

    static void hiss(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Wolf) {
                ((Wolf) i).howl();
            }
        }
    }

    static void screech(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Parrot) {
                ((Parrot) i).screech();
            }
        }
    }

    static void feedWithMeat(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Carnivorous) {
                ((Carnivorous) i).eatMeat();
            }
        }
    }

    static void feedWithPlant(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Herbivorous) {
                ((Herbivorous) i).eatPlant();
            }
        }
    }

    static Animal[] readFromFile(String file) {

        Animal[] animals;
        try (FileReader fr = new FileReader(file);
             Scanner reader = new Scanner(fr);) {
            int count = reader.nextInt();
            animals = new Animal[count];
            for (int i = 0; i < count; i++) {
                String nameClass = reader.next();
                if (nameClass.equals("Wolf")) {
                    Wolf wolf = new Wolf();
                    wolf.setName(reader.next());
                    wolf.setFurColor(reader.next());
                    animals[i] = wolf;
                }
                if (nameClass.equals("Parrot")) {
                    Parrot parrot = new Parrot();
                    parrot.setName(reader.next());
                    parrot.setFeatherolor(reader.next());
                    animals[i] = parrot;

                }
                if (nameClass.equals("Iguana")) {
                    Iguana iguana = new Iguana();
                    iguana.setName(reader.next());
                    iguana.setScaleColor(reader.next());
                    animals[i] = iguana;
                }
            }

        } catch (IOException ex) {

            System.out.println("Huston mamy problem");
            return null;
        }

        return animals;
    }

    static void saveToBinaryFile(Animal[] animals, String file) {

        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos);) {

            oos.writeObject(animals);

        } catch (IOException ex) {
            System.out.println("Problem...");
        }

    }

    static Animal[] readFromBinaryFile(String file) {

        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis);) {

            return (Animal[]) ois.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Problem...");
            return null;
        }
    }
}


