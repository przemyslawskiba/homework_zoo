package zoo;

/**
 * Created by parallels on 6/8/17.
 */
public abstract class Mammal extends Animal{

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
