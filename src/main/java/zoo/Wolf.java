package zoo;

/**
 * Created by parallels on 6/8/17.
 */
public class  Wolf extends Mammal implements Carnivorous {

   public void howl() {
        System.out.println("name: " + getName() + "hovl" );
    }

    @Override
    public void eat() {
        System.out.println("name: " + getName() + "eat");
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + "szarpie wieprza jak Reksiu szynke");
    }
}
