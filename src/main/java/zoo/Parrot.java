package zoo;

public class Parrot extends Bird implements Herbivorous {

    void screech(){
        System.out.println("name:" + getName() + "screech");
    }

    @Override
    public void eat() {
        System.out.println("name: " + getName() + "eat");
    }

    @Override
    public void eatPlant() {
        System.out.println("wpierda*lam plankton");
    }
}
