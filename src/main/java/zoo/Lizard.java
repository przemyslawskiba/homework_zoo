package zoo;

/**
 * Created by parallels on 6/8/17.
 */
public abstract class Lizard extends Animal {

    private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
