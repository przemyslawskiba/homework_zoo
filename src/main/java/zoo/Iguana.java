package zoo;

public class Iguana extends Lizard implements Herbivorous {

    void hiss(){
        System.out.println("name:" + getName() + "hiss");
    }

    @Override
    public void eat() {
        System.out.println("name: " + getName() + "eat");
    }

    @Override
    public void eatPlant() {
        System.out.println("wpierda*lam plankton");
    }
}
