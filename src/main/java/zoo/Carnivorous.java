package zoo;

/**
 * Created by parallels on 6/8/17.
 */
public interface Carnivorous {

    void eatMeat();
}
