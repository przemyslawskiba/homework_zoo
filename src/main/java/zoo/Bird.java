package zoo;

public abstract class Bird extends Animal {

    private String featherolor;

    public String getFeatherolor() {
        return featherolor;
    }

    public void setFeatherolor(String featherolor) {
        this.featherolor = featherolor;
    }
}
